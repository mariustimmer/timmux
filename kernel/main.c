#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <terminal.h>

#if defined(__linux__)
#error "You have to use the cross compiler, not the linux based"
#endif

#if !defined(__i386__)
#error "Please use the i386 target"
#endif

/**
 * Main kernel function which will be called after "booting".
 */
void init_timmos(void) {
    terminal_initialize();
    terminal_writestring("FooBar!\n");
}
