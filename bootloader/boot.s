/**
 * Any information for this multiboot compatible bootloader
 * can be found here: https://wiki.osdev.org/Bare_Bones
 */

/* Constants for the multiboot header */
.set    ALIGN,      1<<0             /* align loaded modules on page boundaries */
.set    MEMINFO,    1<<1             /* provide memory map */
.set    FLAGS,      ALIGN | MEMINFO  /* this is the Multiboot 'flag' field */
.set    MAGIC,      0x1BADB002       /* 'magic number' lets bootloader find the header */
.set    CHECKSUM,   -(MAGIC + FLAGS) /* checksum of above, to prove we are multiboot */

/* Mark the program as a kernel */
.section    .multiboot
.align      4
.long       MAGIC
.long       FLAGS
.long       CHECKSUM

/* Mark the stack */
.section    .bss
.align      16
stack_bottom:
.skip       16384 /* Set the max. stack size to 16 KiB */
stack_top:

/* Define where we will jump to within the kernel */
.section    .text
.global     _start
.type       _start, @function
_start:
    /**
     * Set the register to point to our stack. This hasto be
     * done in assembly since C can not work without a stack.
     */
    mov $stack_top, %esp

    /**
     * @todo:
     * GDT should be loaded here.
     */

    /* Call our kernel main function */
    call init_timmos

    /**
     * Endless loop in case the kernel main function will ever be exited.
     * This is done by disabling all interrupts and then wait for the next
     * interrupt which will never come.
     */
    cli
1:  hlt
    jmp 1b

    /**
     * Set the size of the _start symbol to the current location '.' minus
     * it's start. This is useful when debugging or when you implement call
     * tracing.
     */
    .size   _start, . - _start
