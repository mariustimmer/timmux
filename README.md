TimmOS
======

About
-----
Kernel of the TimmOS operating system.

Building
--------
To build the kernel you don't have to configure anything. Just run the following make commands:
```bash
# In case you need to clean up something from before
make clean

# Make everything
make --always-make -j2 all
```

Author
------
 - [Marius Timmer](https://mariustimmer.de/about/)
