CROSSCOMPILER=~/crosscompile
ASSEMBLY=${CROSSCOMPILER}/bin/i686-elf-as
CC=${CROSSCOMPILER}/bin/i686-elf-gcc
DIRECTORY_BUILD=./build
DIRECTORY_BOOTLOADER=./bootloader
DIRECTORY_KERNEL=./kernel
DIRECTORY_HEADER=${DIRECTORY_KERNEL}/include
DIRECTORY_VARIOUS=./var
SOURCE_BOOTLOADER=${DIRECTORY_BOOTLOADER}/boot.s
SOURCE_KERNEL_MAIN=${DIRECTORY_KERNEL}/main.c
SOURCE_KERNEL_STRING=${DIRECTORY_KERNEL}/string.c
SOURCE_KERNEL_TERMINAL=${DIRECTORY_KERNEL}/terminal.c
SOURCES_KERNEL="${SOURCE_KERNEL_MAIN}" "${SOURCE_KERNEL_STRING}" "${SOURCE_KERNEL_TERMINAL}"
LINKER_SCRIPT=${DIRECTORY_VARIOUS}/linker.ld

bootloader: ${SOURCE_BOOTLOADER}
	mkdir -p ${DIRECTORY_BUILD}
	${ASSEMBLY} \
		-o ${DIRECTORY_BUILD}/boot.o \
		${SOURCE_BOOTLOADER}

kernel:
	mkdir -p ${DIRECTORY_BUILD}
	${CC} \
		-c \
		-o ${DIRECTORY_BUILD}/string.o \
		-I ${DIRECTORY_HEADER} \
		-std=gnu99 \
		-ffreestanding \
		-O2 \
		-Wall \
		-Wextra \
		${SOURCE_KERNEL_STRING}
	${CC} \
		-c \
		-o ${DIRECTORY_BUILD}/terminal.o \
		-I ${DIRECTORY_HEADER} \
		-std=gnu99 \
		-ffreestanding \
		-O2 \
		-Wall \
		-Wextra \
		${SOURCE_KERNEL_TERMINAL}
	${CC} \
		-c \
		-o ${DIRECTORY_BUILD}/main.o \
		-I ${DIRECTORY_HEADER} \
		-std=gnu99 \
		-ffreestanding \
		-O2 \
		-Wall \
		-Wextra \
		${SOURCE_KERNEL_MAIN}

link: bootloader kernel
	${CC} \
		-T ${LINKER_SCRIPT} \
		-o ${DIRECTORY_BUILD}/timmos.bin \
		-ffreestanding \
		-O2 \
		-nostdlib \
		${DIRECTORY_BUILD}/boot.o ${DIRECTORY_BUILD}/main.o ${DIRECTORY_BUILD}/terminal.o ${DIRECTORY_BUILD}/string.o \
		-lgcc

iso: link
	mkdir \
		-p \
		${DIRECTORY_BUILD}/iso/boot/grub
	cp \
		${DIRECTORY_BUILD}/timmos.bin \
		${DIRECTORY_BUILD}/iso/boot/timmos.bin
	echo "menuentry \"TimmOS\" {" > ${DIRECTORY_BUILD}/iso/boot/grub/grub.cfg
	echo "    multiboot /boot/timmos.bin" >> ${DIRECTORY_BUILD}/iso/boot/grub/grub.cfg
	echo "}" >> ${DIRECTORY_BUILD}/iso/boot/grub/grub.cfg
	grub-mkrescue \
		-o ${DIRECTORY_BUILD}/timmos.iso \
		${DIRECTORY_BUILD}/iso

all:
	make clean
	make bootloader
	make kernel
	make link
	make iso

clean:
	rm \
		-rf \
		${DIRECTORY_BUILD}
