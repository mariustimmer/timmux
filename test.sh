#!/bin/bash

CPU=pentium
MEMORY=16M

qemu-system-i386 \
    -cpu "${CPU}" \
    -m "${MEMORY}" \
    -cdrom ./build/timmos.iso
